### Description ###
A primitive Python script for scraping RF signal information from certain Motorola/ARRIS DOCSIS Modems, via the web interface.

### Supported Modems ###
* SB6121
* SB6141

### Python Dependencies ###
[beautifulsoup4](http://www.crummy.com/software/BeautifulSoup/) (available from PyPI or distribution's package manager)

### Usage ###

By default, modem must be reachable at **192.168.100.1**, if different or using SSH tunneling or similar, modify URL and RebootURL appropriately. *To-Do: Make IP address a configurable setting*


```
#!python
MyModem = MotorolaModem( )
```
Create a new instance of the MotorolaModem class


```
#!python
MyModem.getDownstreamInformation( )
```
Returns a list of dictionaries, one per active downstream channel. Each dictionary includes information such as channel ID, power level, signal-to-noise ratio, and frequency.


```
#!python
MyModem.getUpstreamInformation( )
```
Returns a list of dictionaries, one per active upstream channel. Each dictionary includes information such as channel ID, ranging service ID, power level, and frequency.


```
#!python
MyModem.reboot( )
```
Reboots the modem by making a GET request to the modem's *reset.htm* page.