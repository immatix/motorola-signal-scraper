# --------------------------------------------------
#	Motorola SB6121/SB6141/(?) Signal Analyzer
#
# 	Immatix
#	01.15.2016
# --------------------------------------------------
import urllib2
from bs4 import BeautifulSoup

class MotorolaModem(object):
	# -------------------------------------------------------------
	# Name: __init__
	# Arguments: None
	# Purpose: Class constructor
	# -------------------------------------------------------------
	def __init__(self):
		
		# Modem URLs
		# Default IP = 192.168.100.1
		self.URL 			  	= "http://192.168.100.1/cmSignalData.htm"
		self.RebootURL			= "http://192.168.100.1/reset.htm"
		
		# Request cmSignalData.htm and initialize BeautifulSoup
		resp 					= urllib2.urlopen(self.URL)
		body 					= resp.read()
		self.soup   			= BeautifulSoup(body, 'html.parser')
		
		# Parse HTML tables
		self.Downstream			= self.soup.findAll("table")[0]
		self.Upstream			= self.soup.findAll("table")[2]
		self.SignalStats		= self.soup.findAll("table")[3]
		
		# Detect the number of downstream and upstream channels in use
		# by counting table cells.
		self.DownstreamChannels = len(self.Downstream.findAll("tr")[1].findAll("td")) - 1
		self.UpstreamChannels   = len(self.Upstream.findAll("tr")[1].findAll("td")) - 1
		
	
	
	# -------------------------------------------------------------
	# Name: getDownstreamInformation
	# Arguments: None
	# Purpose: Returns a list of dictionaries; one for each
	#		detected downstream channel, containing all information 
	#		including DOCSIS Channel ID, Frequency, 
	#		Signal-to-Noise Ratio, Modulation type, and
	#		Power Level.
	#
	#		Also included are the counts of unerrored, correctable,
	#		and uncorrectable codewords for each downstream
	#		channel.
	# -------------------------------------------------------------	
	def getDownstreamInformation(self):
		
		# Scrape information from HTML tables, by row.
		
		# Downstream Table
		# Row 1 = Channels, Row 2 = Frequencies, Row 3 = SNRs,
		# Row 4 = Modulations, Row 5 = Power Levels
		ChannelIDs  = self.Downstream.findAll("tr")[1].findAll("td", recursive=False)
		Frequencies = self.Downstream.findAll("tr")[2].findAll("td", recursive=False)
		SNRs        = self.Downstream.findAll("tr")[3].findAll("td", recursive=False)
		Modulations = self.Downstream.findAll("tr")[4].findAll("td", recursive=False)
		PowerLevels = self.Downstream.findAll("tr")[5].findAll("td", recursive=False)
		
		# Signal Stats Table
		# Row 2 = Total Unerrored Codewords, Row 3 = Total Correctable Codewords,
		# Row 4 = Total Uncorrectable Codewords
		UnerroredCodewords     = self.SignalStats.findAll("tr")[2].findAll("td", recursive=False)
		CorrectableCodewords   = self.SignalStats.findAll("tr")[3].findAll("td", recursive=False)
		UncorrectableCodewords = self.SignalStats.findAll("tr")[4].findAll("td", recursive=False)
		
		# Empty list and dictionary
		AllChannelInfo     = []
		CurrentChannelInfo = {}
		
		# Loop through all detected channels
		for intChannelIndex in range( 0, self.DownstreamChannels ):
					
			# Put information from current channel into dictionary
			CurrentChannelInfo = { "Channel ID"              : ChannelIDs[intChannelIndex + 1].text.encode("ascii", "ignore").strip(),
								   "Frequency"               : Frequencies[intChannelIndex + 1].text.encode("ascii", "ignore").strip(), 
								   "SNR"                     : SNRs[intChannelIndex + 1].text.encode("ascii", "ignore").strip(), 
								   "Modulation"              : Modulations[intChannelIndex + 1].text.encode("ascii", "ignore").strip(), 
								   "Power Level"             : PowerLevels[intChannelIndex + 1].text.encode("ascii", "ignore").strip(),
								   "Unerrored Codewords"     : UnerroredCodewords[intChannelIndex + 1].text.encode("ascii", "ignore").strip( ),
								   "Correctable Codewords"   : CorrectableCodewords[intChannelIndex + 1].text.encode("ascii", "ignore").strip( ),
								   "Uncorrectable Codewords" : UncorrectableCodewords[intChannelIndex + 1].text.encode("ascii", "ignore").strip( )  }
								   
			# Add dictionary to list
			AllChannelInfo.append(CurrentChannelInfo)
			
		return AllChannelInfo
		
		
		
	# -------------------------------------------------------------
	# Name: getUpstreamInformation
	# Arguments: None
	# Purpose: Returns a list of dictionaries; one for each
	#		detected upstream channel, containing all information 
	#		including DOCSIS Channel ID, Frequency, 
	#		Ranging Service ID, Symbol Rate, and Power Level.
	# -------------------------------------------------------------			
	def getUpstreamInformation(self):
		
		# Scrape information from HTML tables, by row.
		# Row 1 = Channels, Row 2 = Frequencies, Row 3 = Ranging ID,
		# Row 4 = Symbol Rate, Row 5 = Power Level
		ChannelIDs  = self.Upstream.findAll("tr")[1].findAll("td", recursive=False)
		Frequencies = self.Upstream.findAll("tr")[2].findAll("td", recursive=False)
		RangingIDs  = self.Upstream.findAll("tr")[3].findAll("td", recursive=False)
		SymbolRates = self.Upstream.findAll("tr")[4].findAll("td", recursive=False)
		PowerLevels = self.Upstream.findAll("tr")[5].findAll("td", recursive=False)
		
		# Empty list and dictionary
		AllChannelInfo     = []
		CurrentChannelInfo = {}
		
		# Loop through all detected channels
		for intChannelIndex in range( 0, self.UpstreamChannels ):
			
			# Put information from current channel into dictionary
			CurrentChannelInfo = { "Channel ID"  : ChannelIDs[intChannelIndex + 1].text.encode('ascii', "ignore").strip( ),
								   "Frequency"   : Frequencies[intChannelIndex + 1].text.encode("ascii", "ignore").strip( ),
								   "Ranging ID"  : RangingIDs[intChannelIndex + 1].text.encode("ascii", "ignore").strip( ),
								   "Symbol Rate" : SymbolRates[intChannelIndex + 1].text.encode("ascii", "ignore").strip( ),
								   "Power Level" : PowerLevels[intChannelIndex + 1].text.encode("ascii", "ignore").strip( ) }
								   
			# Add dictionary to list
			AllChannelInfo.append( CurrentChannelInfo )
			
		return AllChannelInfo
	
	
	
	# -------------------------------------------------------------
	# Name: reboot
	# Arguments: none
	# Purpose: Makes a GET requet to the modem's reboot URL.
	#		This *should* cause the modem to immediately reboot.
	# -------------------------------------------------------------
	def reboot(self):
		urllib2.urlopen(self.RebootURL)
